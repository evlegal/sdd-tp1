/**
 * @file linkedList_main.c
 * @brief Programme pour les tests de fonctions de gestion de liste chainee
 * ATTENTION : Il faut creer autant de tests que les cas d'execution pour chaque fonction a tester
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "teZZt.h"

#define TMPFILE "temp.tmp"

#ifdef _WIN32 // Ajout de fmemopen pour Windows

FILE *fmemopen(void *buf, size_t size, const char *mode)
{
	FILE *fp;

	(void)size; // avoid warning

	if (strcmp(mode, "r") == 0)
	{
		if ((fp = fopen(TMPFILE, "w")))
		{
			fputs((char *)buf, fp);
			fclose(fp);
		}
		fp = fopen(TMPFILE, "r");
	}
	else if ((fp = fopen(TMPFILE, mode)))
		setbuf(fp, buf);
	return fp;
}
#endif

// Ajout de CheckStr pour simplifier l'écriture des tests
#define CHECKSTR(buf, str)                                                                                               \
	do                                                                                                                   \
	{                                                                                                                    \
		if (0 == strcmp(buf, str))                                                                                       \
		{                                                                                                                \
			++passed_tests;                                                                                              \
			__tezzt__passed_test();                                                                                      \
			fprintf(stderr, "%s:%4d | PASSED  : " __ANSI_GRE__ #buf " == %s\n" __ANSI_NOR__, __FILE__, __LINE__, str);   \
		}                                                                                                                \
		else                                                                                                             \
		{                                                                                                                \
			++failed_tests;                                                                                              \
			__tezzt__failed_test();                                                                                      \
			fprintf(stderr, "%s:%4d | FAILED  : " __ANSI_YEL__ "%s != %s\n" __ANSI_NOR__, __FILE__, __LINE__, buf, str); \
		}                                                                                                                \
	} while (0)

// Exiting instead of returning
#define XREQUIRE(COND)                                                                                       \
	do                                                                                                       \
	{                                                                                                        \
  if (COND)                                                                                                  \
		{                                                                                                    \
			++passed_tests;                                                                                  \
			__tezzt__passed_test();                                                                          \
			fprintf(stderr, "%s:%4d | CHECKED : " __ANSI_GRE__ #COND "\n" __ANSI_NOR__, __FILE__, __LINE__); \
		}                                                                                                    \
		else                                                                                                 \
		{                                                                                                    \
			++failed_tests;                                                                                  \
			__tezzt__failed_test();                                                                          \
			fprintf(stderr, "%s:%4d | REQUIRE : " __ANSI_RED__ #COND "\n" __ANSI_NOR__, __FILE__, __LINE__); \
			exit(1);                                                                                         \
		}                                                                                                    \
	} while (0)

#ifdef _WIN32
char UNCHANGED[] = "unchanged";
#else
char UNCHANGED[] = "\0";
#endif

BEGIN_TEST_GROUP(linkedList)

// TEST UTILS FONCTIONS
cell_t *LL_str(char *str) // LOAD FROM STR
{
	cell_t *l = NULL;
	FILE *f = fmemopen(str, 1024, "r");
	XREQUIRE(NULL != f && NULL != LL_create_list_fromFile(&l, f));
	return l;
}

void LL_cmp(cell_t *l, char *str) // COMPARE WITH STR
{
	char buffer[1024] = {0};
	strcpy(buffer, UNCHANGED);
	FILE *f = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != f);
	LL_save_list_toFile(f, l, NULL);
	fclose(f);
	CHECKSTR(buffer, str);
}

TEST(monom_degree_cmp)
{
	monom_t v1 = {5.11, 7},
			v2 = {3., 5},
			v3 = {5.25, 7};

	printf("\nComparaison des monomes : \n");
	CHECK(monom_degree_cmp(&v1, &v2) > 0);
	CHECK(monom_degree_cmp(&v2, &v1) < 0);
	CHECK(monom_degree_cmp(&v1, &v3) == 0);
}

TEST(monom_save2file)
{
	printf("\nTest sauvegarde de monomes\n");
	monom_t v = {5., 7};

	// creation du flux de texte => buffer
	char buffer[1024] = {0};
	FILE *file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);

	monom_save2file(file, &v);

	fclose(file);

	CHECKSTR(buffer, "5.000 7\n");
	strcpy(buffer, UNCHANGED);

	// Test with invalid call
	monom_save2file(NULL, &v);

	CHECKSTR(buffer, UNCHANGED); // unchanged if file=NULL

	monom_save2file(file, NULL);

	CHECKSTR(buffer, UNCHANGED); // unchanged if mn=NULL
}

TEST(LL_init_list)
{
	cell_t *list;

	// Init with void list
	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);
	REQUIRE(list == NULL);

	// Init with invalid call
	LL_init_list(NULL);
	CHECK(1); // no crash = success
}

TEST(LL_create_cell)
{ // test de creation de cellule
	cell_t *new = NULL;
	monom_t m1 = {3.245, 17};

	printf("\nCreate a new cell (3.245 17) : \n");
	new = LL_create_cell(&m1);
	REQUIRE(NULL != new);
	CHECK(NULL == new->next);
	CHECK(m1.coef == new->val.coef);
	CHECK(m1.degree == new->val.degree);
	free(new);

	// Test with invalid call
	CHECK(NULL == LL_create_cell(NULL));
}

// test d'insertion de cellule - liste a une cellule
TEST(LL_add_cell1)
{
	cell_t *list = NULL,
		   *new = NULL;
	monom_t m1 = {3.45, 17};

	printf("\nAdd a cell to a linked list : \n");

	// regular test: add in void list
	new = LL_create_cell(&m1);
	REQUIRE(new != NULL);

	LL_add_cell(&list, new);
	CHECK(list == new);

	CHECK(list->val.coef == m1.coef);
	CHECK(list->val.degree == m1.degree);
	CHECK(list->next == NULL);

	// invalid call test
	LL_add_cell(&list->next, NULL);
	CHECK(list->next == NULL);

	LL_add_cell(NULL, new);
	CHECK(list->next == NULL);
	CHECK(new->next == NULL);

	LL_add_cell(NULL, NULL);
	CHECK(1); // no crash = success

	free(list); // liberer la cellule
}

// test d'insertion de cellule - liste a deux cellules
TEST(LL_add_cell2)
{
	cell_t *list = NULL,
		   *new = NULL;
	monom_t m1 = {3.45, 17},
			m2 = {25.8, 9};

	printf("\nAdd two cells to a linked list : \n");

	new = LL_create_cell(&m1);
	REQUIRE(new != NULL);
	LL_add_cell(&list, new); // add in void list
	CHECK(list == new);

	new = LL_create_cell(&m2);
	REQUIRE(new != NULL);
	LL_add_cell(&list, new); // add at start
	CHECK(list == new);

	// tester les valeurs de la liste
	CHECK(list->val.coef == m2.coef); // 25.8 est une valeur non approchee
	CHECK(list->val.degree == m2.degree);
	REQUIRE(list->next != NULL);

	CHECK(list->next->val.coef == m1.coef); // 3.45 est une valeur non approchee
	CHECK(list->next->val.degree == m1.degree);
	CHECK(list->next->next == NULL);

	// liberer la liste
	free(list->next);
	free(list);
}

// test d'insertion de cellule - liste a trois cellules
TEST(LL_add_cell3)
{
	cell_t *list = NULL,
		   *new = NULL;
	monom_t m1 = {3.245, 17},
			m2 = {25.8, 9},
			m3 = {12.4, 3};

	printf("\nAdd three cells to a linked list : \n");

	new = LL_create_cell(&m1); // L: NULL  =>  L: m1
	REQUIRE(new != NULL);
	LL_add_cell(&list, new); // add in void list
	CHECK(list == new);

	new = LL_create_cell(&m2); // L: m1  =>  L: m1 -> m2
	REQUIRE(new != NULL);
	LL_add_cell(&list->next, new); // add at end
	CHECK(list->next == new);

	// ajouter le m3 en 2e position de la liste
	new = LL_create_cell(&m3); // L: m1 -> m2  =>  L: m1 -> m3 -> m2
	REQUIRE(new != NULL);
	LL_add_cell(&list->next, new); // add at middle
	CHECK(list->next == new);

	// tester les valeurs de la liste: m1 -> m3 -> m2
	CHECK(list->val.coef == m1.coef);
	CHECK(list->val.degree == m1.degree);
	REQUIRE(list->next != NULL);

	CHECK(list->next->val.coef == m3.coef);
	CHECK(list->next->val.degree == m3.degree);
	REQUIRE(list->next->next != NULL);

	CHECK(list->next->next->val.coef == m2.coef);
	CHECK(list->next->next->val.degree == m2.degree);
	CHECK(list->next->next->next == NULL);

	// liberer la liste
	free(list->next->next);
	free(list->next);
	free(list);
}

TEST(LL_save_list_toFile)
{ // test pour l'ecriture d'un polynome sur un flux de sortie
	cell_t *list = NULL,
		   *new = NULL;
	monom_t m1 = {3.245, 17},
			m2 = {25.8, 9},
			m3 = {12.4, 3};

	printf("\ntest for saving polynomial in file\n");

	char buffer[1024] = "";
	FILE *file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);

	new = LL_create_cell(&m1);
	REQUIRE(new != NULL);
	LL_add_cell(&list, new);
	CHECK(list == new);

	new = LL_create_cell(&m2);
	REQUIRE(new != NULL);
	LL_add_cell(&list, new);
	CHECK(list == new);

	// ajouter le m3 en tete de la liste
	new = LL_create_cell(&m3);
	REQUIRE(new != NULL);
	LL_add_cell(&list, new);
	CHECK(list == new);

	LL_save_list_toFile(file, list, NULL);

	fclose(file);

	CHECKSTR(buffer, "12.400 3\n25.800 9\n3.245 17\n");

	// test with invalid call

	LL_save_list_toFile(NULL, list, NULL);
	CHECK(1); // no crash = success

	strcpy(buffer, UNCHANGED);
	file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);
	LL_save_list_toFile(file, NULL, NULL);
	fclose(file);
	CHECKSTR(buffer, UNCHANGED);

	LL_save_list_toFile(NULL, NULL, NULL);
	CHECK(1); // no crash = success

	free(list->next->next);
	free(list->next);
	free(list);
}

// test pour la creation d'un polynome a partir d'un fichier
TEST(LL_create_list_fromFileName0)
{
	cell_t *list = NULL;

	printf("\nCreate a linked list from file name0: \n");

	CHECK(NULL == LL_create_list_fromFileName(&list, "notExist.txt"));
	CHECK(NULL == list);

	CHECK(NULL == LL_create_list_fromFileName(&list, NULL));
	CHECK(NULL == list);

	// test with invalid call
	CHECK(NULL == LL_create_list_fromFileName(NULL, NULL));

	CHECK(NULL == LL_create_list_fromFileName(NULL, "notExist.txt"));
}

// test pour la creation d'un polynome a partir d'un fichier (flux)
TEST(LL_create_list_fromFile)
{
	cell_t *list = NULL;

	//////////////// Test with regular call
	printf("\nCreate a linked list from file: \n");

	char buffer[1024] = "12.400 3\n25.800 9\n3.245 17\n";
	FILE *file = fmemopen(buffer, 1024, "r");

	REQUIRE(NULL != file);

	CHECK(LL_create_list_fromFile(&list, file) == list);
	REQUIRE(NULL != list);

	// tester les valeurs de la liste
	CHECK(list->val.coef == 12.4); // 12.4 est une valeur non approchee
	CHECK(list->val.degree == 3);
	REQUIRE(list->next != NULL);

	CHECK(list->next->val.coef == 25.8); // 25.8 est une valeur non approchee
	CHECK(list->next->val.degree == 9);
	REQUIRE(list->next->next != NULL);

	CHECK(list->next->next->val.coef == 3.245); // 3.245 est une valeur non approchee
	CHECK(list->next->next->val.degree == 17);
	CHECK(list->next->next->next == NULL);

	// liberer la liste
	free(list->next->next);
	free(list->next);
	free(list);
	list = NULL;

	//////////////// Test with partially invalid file  ->__
	sprintf(buffer, "12.400 3\n25.800 9\n3.245 17\n7.566 AZ\n3.245 17\n");
	file = fmemopen(buffer, 1024, "r");
	REQUIRE(NULL != file);

	list = LL_create_list_fromFile(NULL, file); // Additionnal test here with adrhead=NULL
	REQUIRE(NULL != list);

	// tester les valeurs de la liste
	CHECK(list->val.coef == 12.4); // 12.4 est une valeur non approchee
	CHECK(list->val.degree == 3);
	REQUIRE(list->next != NULL);

	CHECK(list->next->val.coef == 25.8); // 25.8 est une valeur non approchee
	CHECK(list->next->val.degree == 9);
	REQUIRE(list->next->next != NULL);

	CHECK(list->next->next->val.coef == 3.245); // 3.245 est une valeur non approchee
	CHECK(list->next->next->val.degree == 17);
	CHECK(list->next->next->next == NULL);

	// liberer la liste
	free(list->next->next);
	free(list->next);
	free(list);
	list = NULL;

	//////////////// Test with invalid file
	sprintf(buffer, "A\n");
	file = fmemopen(buffer, 1024, "r");
	REQUIRE(NULL != file);

	CHECK(NULL == LL_create_list_fromFile(NULL, file));

	//////////////// Test with no file
	CHECK(NULL == LL_create_list_fromFile(NULL, NULL));
}

TEST(LL_free_list)
{ // test de la liberation de liste
	printf("\test de la liberation de liste\n");
	cell_t *list = LL_str("42.000 1\n12.400 3\n42.000 7\n25.800 9\n3.245 17\n42.000 19\n");

	REQUIRE(NULL != list);
	REQUIRE(NULL != list->next);
	REQUIRE(NULL != list->next->next);
	REQUIRE(NULL != list->next->next->next);
	REQUIRE(NULL != list->next->next->next->next);
	REQUIRE(NULL != list->next->next->next->next->next);

	LL_free_list(&list); // complément avec valgrind
	CHECK(list == NULL);

	LL_free_list(&list);
	CHECK(1); // no crash

	LL_free_list(NULL);
	CHECK(1); // no crash
}

TEST(LL_str_cmp)
{
	// test des fonctions tests
	// ces fonctions utilisent LL_create_list_fromFile et LL_save_list_toFile qui viennent d'etre testé
	// ces fonctions serviront pour facilité l'écriture et la comprehension des tests suivants.

	// leur code est suffisament explicite pour voir que ces fonctions sont correctes (sans tests faussement positifs)
	printf("\ntest des fonctions de tests\n");

	cell_t *l = LL_str("12.400 3\n");
	LL_cmp(l, "12.400 3\n");
	LL_free_list(&l);

	/*
	l = LL_str("12.400 3\n");
	LL_cmp(l, "15.000 3\n");
	LL_free_list(&l);*/
	// successfully failed -> the test wont pass, means its correct

	l = LL_str("12.400 3\n25.800 9\n3.245 17\n");
	LL_cmp(l, "12.400 3\n25.800 9\n3.245 17\n");
	LL_free_list(&l);

	l = LL_str("12.400 3\n25.800 9\n3.245 17\n3.245 AZ\n3.245 17\n");
	LL_cmp(l, "12.400 3\n25.800 9\n3.245 17\n");
	LL_free_list(&l);
}

TEST(LL_save_and_load_list_toFileName)
{
	printf("\ntest of saving and loading polynomials in real file\n");

	remove("test.txt");

	cell_t *l = LL_str("12.400 3\n25.800 9\n3.245 17\n");

	LL_save_list_toFileName("test.txt", l, NULL);

	REQUIRE(LL_create_list_fromFileName(&l, "test.txt") != NULL);
	LL_cmp(l, "12.400 3\n25.800 9\n3.245 17\n");
	LL_free_list(&l);

	char buffer[1024] = {0};
	FILE *file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);
	fputs(UNCHANGED, file);
	LL_save_list_toFile(file, l, NULL); // test with void list
	fclose(file);
	CHECKSTR(buffer, UNCHANGED);

	file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);
	fputs(UNCHANGED, file);
	LL_save_list_toFile(file, NULL, NULL); // test with NULL list
	fclose(file);
	CHECKSTR(buffer, UNCHANGED);

	file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);
	fputs(UNCHANGED, file);
	LL_save_list_toFile(NULL, l, NULL); // test with NULL file
	fclose(file);
	CHECKSTR(buffer, UNCHANGED);

	file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);
	fputs(UNCHANGED, file);
	LL_save_list_toFile(NULL, NULL, NULL); // test with NULL file and NULL list
	fclose(file);
	CHECKSTR(buffer, UNCHANGED);

	remove("test.txt");
}

TEST(LL_add_cellfree)
{ // test (cas non standard) d'insertion de list dans une list

	printf("\ntest (cas non standard) d'insertion de list dans une list\n");

	cell_t *l1 = LL_str("12.400 3\n25.800 9\n3.245 17\n"),
		   *l2 = LL_str("12.400 20\n25.800 21\n3.245 24\n");

	LL_add_cell(&(l1->next->next->next), l2->next);
	LL_cmp(l1, "12.400 3\n25.800 9\n3.245 17\n25.800 21\n");
	LL_cmp(l2, "12.400 20\n25.800 21\n");

	LL_free_list(&l1);

	// LL_free_list(&l2);
	free(l2); // LL_free_list ne marche pas ici sur l2 car la 2e cellule à déjà été free, effet secondaire normal!
}

TEST(LL_search_prev)
{ // test pour la fonction de recherche d'une valeur
	printf("\ntest of searching function\n");

	cell_t *list = LL_str("12.400 3\n25.800 9\n3.245 17\n");
	monom_t mn = {42., 2};

	cell_t **sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list);

	mn.degree = 3;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list);

	mn.degree = 5;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next);

	mn.degree = 9;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next);

	mn.degree = 11;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next);

	mn.degree = 17;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next);

	mn.degree = 20;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next->next && *sr == NULL);

	// test with invalid call
	CHECK(&list == LL_search_prev(&list, NULL, NULL));
	CHECK(NULL == LL_search_prev(NULL, &mn, NULL));
	CHECK(NULL == LL_search_prev(NULL, NULL, NULL));

	LL_free_list(&list);
}

TEST(LL_add_celln)
{ // test d'insertion de cellule - liste a n cellules
	printf("\ntest of insertion functions\n");
	cell_t *list = LL_str("12.400 3\n25.800 9\n3.245 17\n");
	monom_t mn = {42., 7};

	// insert middle
	cell_t **sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next);

	cell_t *ins = LL_create_cell(&mn);
	REQUIRE(NULL != ins);

	LL_add_cell(sr, ins);
	CHECK(list->next == ins);

	LL_cmp(list, "12.400 3\n42.000 7\n25.800 9\n3.245 17\n");

	// insert start
	mn.degree = 1;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list);

	ins = LL_create_cell(&mn);
	REQUIRE(NULL != ins);

	LL_add_cell(sr, ins);
	CHECK(list == ins);

	LL_cmp(list, "42.000 1\n12.400 3\n42.000 7\n25.800 9\n3.245 17\n");

	// insert end
	mn.degree = 19;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next->next->next->next);

	ins = LL_create_cell(&mn);
	REQUIRE(NULL != ins);

	LL_add_cell(sr, ins);
	CHECK(list->next->next->next->next->next == ins);

	LL_cmp(list, "42.000 1\n12.400 3\n42.000 7\n25.800 9\n3.245 17\n42.000 19\n");

	LL_free_list(&list);
}

TEST(LL_del_cell)
{ // test de la suppression d'un element
	printf("\ntest of deletetion function\n");
	cell_t *list = LL_str("42.000 1\n12.400 3\n42.000 7\n25.800 9\n3.245 17\n42.000 19\n");
	monom_t mn = {42., 7};

	// del middle
	cell_t **sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next);

	LL_del_cell(sr);

	LL_cmp(list, "42.000 1\n12.400 3\n25.800 9\n3.245 17\n42.000 19\n");

	// del start
	mn.degree = 1;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list);

	LL_del_cell(sr);

	LL_cmp(list, "12.400 3\n25.800 9\n3.245 17\n42.000 19\n");

	// del end
	mn.degree = 19;
	sr = LL_search_prev(&list, &mn, NULL);
	CHECK(*sr == list->next->next->next);

	LL_del_cell(sr);

	LL_cmp(list, "12.400 3\n25.800 9\n3.245 17\n");

	LL_free_list(&list);

	// test with invalid call
	LL_del_cell(NULL);
}

END_TEST_GROUP(linkedList)

int main(void)
{
	RUN_TEST_GROUP(linkedList);
	remove(TMPFILE); // remove windows tmp file
	return EXIT_SUCCESS;
}
