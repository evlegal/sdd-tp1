/**
 * @file valCell.h
 * @brief relative to the value of linked list's cells
 */

#ifndef __VALCELL_H__
#define __VALCELL_H__

#include <stdio.h>

/**
 * @struct monom_t
 * @brief Data structure of monomial (value of linked list's cell)
 * @var monom_t::coef
 * coefficient of the monom : double precision real
 * @var monom_t::degree
 * degree of the monom : unsigned integer
 */
typedef struct monom_t
{
    double coef;
    unsigned int degree;
} monom_t;

/**
 * @brief Compare the degree of two monomials
 * @param [in] m1 address of the first monomial
 * @param [in] m2 address of the second monomial
 * @return -1 if degree1<degree2; 0 if degree1=degree2;  1 if degree1>degree2 (more useful than <0 / >0 in switch statement)
 * @note Lexique: None
 */
int monom_degree_cmp(monom_t *m1, monom_t *m2);

/**
 * @brief write the information of a monomial to the given output stream
 * @param [in] file file pointer of an output stream
 * @param [in] mn address of a monomial
 * @details if file or mn is NULL, nothing is done
 * @note Lexique: None
 */
void monom_save2file(FILE *file, monom_t *mn);

#endif