/**
 * @file polynomial.h
 * @brief Header file of polynomial module
 */

#ifndef __POLYNOMIAL_H__
#define __POLYNOMIAL_H__

#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

/**
 * @brief compute 'in place' the derive of a polynomial
 * @param [in, out] adrhead address of a polynomial's adrhead pointer
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @details output only if there are not any error
 * @note Lexique:
 *          - cell: address of cell (used as iterator)
 */
void poly_derive(cell_t **adrhead, FILE *output);

/**
 * @brief compute P1 = P1 + P2, P2 become empty
 * @param [in, out] P1 address of the 1st polynomial's head pointer
 * @param [in, out] P2 address of the 2nd polynomial's head pointer
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @details if any P1 or P2 is NULL, nothing is done
 * @details output only if there are not any error
 * @note Lexique:
 *          - currt: address of cell (used as tmp)
 *          - pprev1: address of a P1's cell pointer, used to iter over P1
 *          - pprev2: address of a P2's cell pointer, used to iter over P2
 */
void poly_add(cell_t **P1, cell_t **P2, FILE *output);

/**
 * @brief compute P1 * P2
 * @param [in] P1 head pointer of the 1st polynomial
 * @param [in] P2 head pointer of the 2nd polynomial
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @return P3 = P1 * P2
 * @details if any P1 or P2 is NULL, NULL is returned
 * @details if a memory alloc fails, NULL is returned
 * @details P1 and P2 are never altered
 * @details output only if there are not any error
 * @note Lexique:
 *          - P3: head pointer of the returned product
 *          - curr1: cell pointer, used as iterator over P1
 *          - curr2: cell pointer, used as iterator over P2
 *          - pprev3: address of a cell pointer, used as iterator over &P3
 *          - insert: used when inserting a new cell
 *          - pprev: used to keep order of monoms in P3
 *          - mn: monom used to compute monom of the product
 *          - alloc_error: used to interrupt the process at any alloc error
 */
cell_t *poly_prod(cell_t *P1, cell_t *P2, FILE *output);

#endif