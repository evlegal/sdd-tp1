var searchData=
[
  ['poly_5fadd_20',['poly_add',['../polynomial_8c.html#abae92f7cbd91f6ae8c15b3214a4e0dd8',1,'poly_add(cell_t **P1, cell_t **P2, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#abae92f7cbd91f6ae8c15b3214a4e0dd8',1,'poly_add(cell_t **P1, cell_t **P2, FILE *output):&#160;polynomial.c']]],
  ['poly_5fderive_21',['poly_derive',['../polynomial_8c.html#ac69f64e4081c20e94a26d8730f254977',1,'poly_derive(cell_t **adrhead, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#ac69f64e4081c20e94a26d8730f254977',1,'poly_derive(cell_t **adrhead, FILE *output):&#160;polynomial.c']]],
  ['poly_5fprod_22',['poly_prod',['../polynomial_8c.html#af121951e939e3c8a5b8702b70c87fb2d',1,'poly_prod(cell_t *P1, cell_t *P2, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#af121951e939e3c8a5b8702b70c87fb2d',1,'poly_prod(cell_t *P1, cell_t *P2, FILE *output):&#160;polynomial.c']]],
  ['polynomial_2ec_23',['polynomial.c',['../polynomial_8c.html',1,'']]],
  ['polynomial_2eh_24',['polynomial.h',['../polynomial_8h.html',1,'']]],
  ['polynomial_5fmain_2ec_25',['polynomial_main.c',['../polynomial__main_8c.html',1,'']]]
];
