var searchData=
[
  ['poly_5fadd_52',['poly_add',['../polynomial_8c.html#abae92f7cbd91f6ae8c15b3214a4e0dd8',1,'poly_add(cell_t **P1, cell_t **P2, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#abae92f7cbd91f6ae8c15b3214a4e0dd8',1,'poly_add(cell_t **P1, cell_t **P2, FILE *output):&#160;polynomial.c']]],
  ['poly_5fderive_53',['poly_derive',['../polynomial_8c.html#ac69f64e4081c20e94a26d8730f254977',1,'poly_derive(cell_t **adrhead, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#ac69f64e4081c20e94a26d8730f254977',1,'poly_derive(cell_t **adrhead, FILE *output):&#160;polynomial.c']]],
  ['poly_5fprod_54',['poly_prod',['../polynomial_8c.html#af121951e939e3c8a5b8702b70c87fb2d',1,'poly_prod(cell_t *P1, cell_t *P2, FILE *output):&#160;polynomial.c'],['../polynomial_8h.html#af121951e939e3c8a5b8702b70c87fb2d',1,'poly_prod(cell_t *P1, cell_t *P2, FILE *output):&#160;polynomial.c']]]
];
