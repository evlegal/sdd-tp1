/**
 * @file valCell.c
 * @brief Implementation file for comparison and display of linked list's cells
 */

#include "valCell.h"

/**
 * @brief Compare the degree of two monomials
 * @param [in] m1 address of the first monomial
 * @param [in] m2 address of the second monomial
 * @return -1 if degree1<degree2; 0 if degree1=degree2;  1 if degree1>degree2 (more useful than <0 / >0 in switch statement)
 * @note Lexique: None
 */ 
int monom_degree_cmp(monom_t * m1, monom_t * m2)
{
 	// return (int)m1->degree - (int)m2->degree; (not usable in switch statement)
	return (m1->degree < m2->degree) ? -1: !(m1->degree == m2->degree);
}


/**
 * @brief write the information of a monomial to the given output stream
 * @param [in] file file pointer of an output stream
 * @param [in] mn address of a monomial
 * @details if file or mn is NULL, nothing is done
 * @note Lexique: None
 */
void monom_save2file(FILE * file, monom_t * mn)
{
	if (mn && file) fprintf(file, "%.3f %u\n", mn->coef, mn->degree);
}