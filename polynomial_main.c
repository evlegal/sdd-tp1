/**
 * @file polynomial_main.c
 * @brief Programme pour les tests des operations sur les polynomes
 * ATTENTION : Il faut creer autant de tests que les cas d'execution pour chaque fonction a tester
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "polynomial.h"
#include "teZZt.h"

#define TMPFILE "temp.tmp"

#ifdef _WIN32 // Ajout de fmemopen pour Windows

FILE *fmemopen(void *buf, size_t size, const char *mode)
{
	FILE *fp;

	(void)size; // Avoid warning

	if (strcmp(mode, "r") == 0)
	{
		if ((fp = fopen(TMPFILE, "w")))
		{
			fputs((char *)buf, fp);
			fclose(fp);
		}
		fp = fopen(TMPFILE, "r");
	}
	else if ((fp = fopen(TMPFILE, mode)))
		setbuf(fp, buf);
	return fp;
}
#endif

// Ajout de CheckStr pour simplifier l'écriture des tests
#define CHECKSTR(buf, str)                                                                                                       \
	do                                                                                                                           \
	{                                                                                                                            \
		if (0 == strcmp(buf, str))                                                                                               \
		{                                                                                                                        \
			++passed_tests;                                                                                                      \
			__tezzt__passed_test();                                                                                              \
			fprintf(stderr, "%s:%4d | PASSED  : " __ANSI_GRE__ #buf " == \"%s\"\n" __ANSI_NOR__, __FILE__, __LINE__, str);       \
		}                                                                                                                        \
		else                                                                                                                     \
		{                                                                                                                        \
			++failed_tests;                                                                                                      \
			__tezzt__failed_test();                                                                                              \
			fprintf(stderr, "%s:%4d | FAILED  : " __ANSI_YEL__ "\"%s\" != \"%s\"\n" __ANSI_NOR__, __FILE__, __LINE__, buf, str); \
		}                                                                                                                        \
	} while (0)

// Exiting instead of returning
#define XREQUIRE(COND)                                                                                       \
	do                                                                                                       \
	{                                                                                                        \
  if (COND)                                                                                                  \
		{                                                                                                    \
			++passed_tests;                                                                                  \
			__tezzt__passed_test();                                                                          \
			fprintf(stderr, "%s:%4d | CHECKED : " __ANSI_GRE__ #COND "\n" __ANSI_NOR__, __FILE__, __LINE__); \
		}                                                                                                    \
		else                                                                                                 \
		{                                                                                                    \
			++failed_tests;                                                                                  \
			__tezzt__failed_test();                                                                          \
			fprintf(stderr, "%s:%4d | REQUIRE : " __ANSI_RED__ #COND "\n" __ANSI_NOR__, __FILE__, __LINE__); \
			exit(1);                                                                                         \
		}                                                                                                    \
	} while (0)

#ifdef _WIN32
char UNCHANGED[] = "unchanged";
#else
char UNCHANGED[] = "\0";
#endif

BEGIN_TEST_GROUP(polynomial)

// TEST UTILS FONCTIONS
cell_t *LL_str(char *str) // LOAD FROM STR
{
	cell_t *l = NULL;
	FILE *f = fmemopen(str, 1024, "r");
	XREQUIRE(NULL != f && NULL != LL_create_list_fromFile(&l, f));
	printf(">> %p\n", l);
	return l;
}

void LL_cmp(cell_t *l, char *str) // COMPARE WITH STR
{
	char buffer[1024] = {0};
	strcpy(buffer, UNCHANGED);
	FILE *f = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != f);
	LL_save_list_toFile(f, l, NULL);
	fclose(f);
	CHECKSTR(buffer, str);
}

TEST(LL_init_list)
{
	cell_t *list;

	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);

	REQUIRE(list == NULL);
}

TEST(Poly_derive)
{ // test sur la derivation d'un polynome
	printf("\nDerive of polynomial : \n");
	cell_t *poly = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n");

	poly_derive(&poly, NULL);

	LL_cmp(poly, "5.000 0\n8.000 1\n15.000 2\n24.000 3\n15.000 4\n"); //"(5.00, 1) (4.00, 2) (5.00, 3) (6.00, 4) (3.00, 5) ") );
	LL_free_list(&poly);

	poly = LL_str("2.000 0\n"); // test with 1 monom (degree 0)

	poly_derive(&poly, NULL);

	LL_cmp(poly, UNCHANGED);
	LL_free_list(&poly);

	poly = LL_str("2.000 42\n"); // test with 1 monom (degree non-0)

	poly_derive(&poly, NULL);

	LL_cmp(poly, "84.000 41\n");
	LL_free_list(&poly);

	poly = NULL; // test with void list

	poly_derive(&poly, NULL);

	LL_cmp(poly, UNCHANGED);
	LL_free_list(&poly);
}

TEST(Poly_addition)
{ // test sur l'addition de deux polymones
	printf("\nAddition of polynomials : \n");

	// test avec polynome égaux
	printf("test avec polynome égaux\n");
	cell_t *poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
		   *poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "4.000 0\n10.000 1\n8.000 2\n10.000 3\n12.000 4\n6.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec insertion milieu P1
	printf("test avec insertion milieu P1\n");
	poly1 = LL_str("2.000 0\n4.000 2\n5.000 3\n3.000 5\n"),
	poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "4.000 0\n5.000 1\n8.000 2\n10.000 3\n6.000 4\n6.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec insertion au extremités (+ milieu) P1
	printf("test avec insertion au extremités (+ milieu) P1\n");
	poly1 = LL_str("5.000 3\n"),
	poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "2.000 0\n5.000 1\n4.000 2\n10.000 3\n6.000 4\n3.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec insertion au extrémité + milieu (P2)
	printf("test avec insertion au extrémité + milieu (P2)\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
	poly2 = LL_str("2.000 0\n4.000 2\n5.000 3\n3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "4.000 0\n5.000 1\n8.000 2\n10.000 3\n6.000 4\n6.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec insertion au milieu (P2)
	printf("test avec insertion au milieu (P2)\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
	poly2 = LL_str("5.000 3\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "2.000 0\n5.000 1\n4.000 2\n10.000 3\n6.000 4\n3.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec annulation des monomes au milieu
	printf("test avec annulation des monomes au milieu\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
	poly2 = LL_str("2.000 0\n-5.000 1\n-4.000 2\n5.000 3\n-6.000 4\n3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "4.000 0\n10.000 3\n6.000 5\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec annulation des monomes aux extrémités
	printf("test avec annulation des monomes aux extrémitéss\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
	poly2 = LL_str("-2.000 0\n-5.000 1\n4.000 2\n5.000 3\n6.000 4\n-3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "8.000 2\n10.000 3\n12.000 4\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec annulation de tout les monomes
	printf("test avec annulation de tout les monomes\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n5.000 3\n6.000 4\n3.000 5\n"),
	poly2 = LL_str("-2.000 0\n-5.000 1\n-4.000 2\n-5.000 3\n-6.000 4\n-3.000 5\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, UNCHANGED);
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec P1 NULL
	printf("test avec P1 NULL\n");
	poly1 = NULL,
	poly2 = LL_str("5.000 3\n5.000 4\n");

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "5.000 3\n5.000 4\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec P2 NULL
	printf("test avec P2 NULL\n");
	poly1 = LL_str("5.000 3\n5.000 4\n"),
	poly2 = NULL;

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, "5.000 3\n5.000 4\n");
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	// test avec P1 & P2 NULL
	printf(" test avec P1 & P2 NULL\n");
	poly1 = NULL,
	poly2 = NULL;

	poly_add(&poly1, &poly2, NULL);

	LL_cmp(poly1, UNCHANGED); // void
	LL_cmp(poly2, UNCHANGED); // void
	CHECK(poly1 == NULL);
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);
}

TEST(Poly_produit)
{ // test sur le calcul du produit de deux polymones
	printf("\nProduct of polynomials : \n");

	// test with 1 monom (diff degree)
	printf("test with 1 monom (diff degree)\n");
	cell_t *poly1 = LL_str("2.000 1\n"),
		   *poly2 = LL_str("5.000 2\n"),
		   *poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "10.000 3\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test with 1 monom (same degree)
	printf("test with 1 monom (same degree)\n");
	poly1 = LL_str("2.000 2\n"),
	poly2 = LL_str("5.000 2\n"),
	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "10.000 4\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test polynomial shift
	printf("test polynomial shift\n");
	poly1 = LL_str("1.000 42\n"),
	poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n"),
	poly3 = NULL;

	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "2.000 42\n5.000 43\n4.000 44\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test polynomial (diff degree)
	printf("test polynomial (diff degree)\n");
	poly1 = LL_str("2.000 3\n5.000 4\n4.000 5\n"),
	poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n"),
	poly3 = NULL;

	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "4.000 3\n20.000 4\n41.000 5\n40.000 6\n16.000 7\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test polynomial (same degree)
	printf("test polynomial (same degree)\n");
	poly1 = LL_str("2.000 0\n5.000 1\n4.000 2\n"),
	poly2 = LL_str("2.000 0\n5.000 1\n4.000 2\n"),
	poly3 = NULL;

	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "4.000 0\n20.000 1\n41.000 2\n40.000 3\n16.000 4\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test with 0 coeffed monom: (ax+b)(ax-b) = a²x² + 0x - b²
	printf("test with 0 coeffed monom: (ax+b)(ax-b) = a²x² + 0x - b²\n");
	poly1 = LL_str("2.000 0\n5.000 1\n"),
	poly2 = LL_str("-2.000 0\n5.000 1\n"),
	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, "-4.000 0\n25.000 2\n");

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test with void P1
	printf("test with void P1\n");
	poly1 = NULL,
	poly2 = LL_str("-2.000 0\n5.000 1\n"),
	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, UNCHANGED);
	CHECK(poly3 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test with void P2
	printf("test with void P2\n");
	poly2 = NULL,
	poly1 = LL_str("-2.000 0\n5.000 1\n"),
	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, UNCHANGED);
	CHECK(poly3 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

	// test with void P1 & P2
	printf("test with void P2\n");
	poly2 = NULL,
	poly1 = NULL,
	poly3 = poly_prod(poly1, poly2, NULL);

	LL_cmp(poly3, UNCHANGED);
	CHECK(poly3 == NULL);

	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);
}

TEST(LL_save_list_toFileName)
{ // test pour l'ecriture d'un polynome dans un fichier

	char buffer[1024] = {0};

	// test on derive
	FILE *f = fmemopen(buffer, 1024, "w");
	REQUIRE(f != NULL);

	cell_t *l = LL_str("-2.000 0\n5.000 1\n5.000 5\n");
	poly_derive(&l, f);
	fclose(f);
	CHECKSTR(buffer, "5.000 0\n25.000 4\n");
	LL_cmp(l, buffer);

	// test on add
	f = fmemopen(buffer, 1024, "w");
	REQUIRE(f != NULL);

	cell_t *l2 = LL_str("-2.000 0\n5.000 1\n5.000 5\n");
	poly_add(&l, &l2, f);
	fclose(f);

	CHECKSTR(buffer, "3.000 0\n5.000 1\n25.000 4\n5.000 5\n");
	LL_cmp(l, buffer);
	LL_cmp(l2, UNCHANGED);

	// test on product
	f = fmemopen(buffer, 1024, "w");
	REQUIRE(f != NULL);

	l2 = poly_prod(l, l, f);
	fclose(f);
	// 25 x^10 + 250 x^9 + 625 x^8 + 50 x^6 + 280 x^5 + 150 x^4 + 25 x^2 + 30 x + 9
	CHECKSTR(buffer, "9.000 0\n30.000 1\n25.000 2\n150.000 4\n280.000 5\n50.000 6\n625.000 8\n250.000 9\n25.000 10\n");
	LL_cmp(l2, buffer);

	LL_free_list(&l);
	LL_free_list(&l2);
}

END_TEST_GROUP(polynomial)

int main(void)
{
	RUN_TEST_GROUP(polynomial);
	remove(TMPFILE);
	return EXIT_SUCCESS;
}
