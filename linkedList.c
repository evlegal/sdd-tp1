/**
 * @file linkedList.c
 * @brief Implementation file of linked list module
 */

#include "linkedList.h"

/**
 * @brief Initialize a void list
 * @param [in, out] adrhead address of head pointer of the list
 * @details nothing is done if adrhead is NULL
 * @details *adrhead must not to point to data, because it won't be freed here
 * @note Lexique: None
 */
void LL_init_list(cell_t **adrhead)
{
    if (adrhead)
        *adrhead = NULL;
}

/**
 * @brief Create a new cell for linked list from its data
 * @param [in] monom address of the data
 * @return address of the new cell (or NULL if memory failure)
 * @details if monom is NULL, NULL will be returned
 * @note Lexique:
 *          - cell: the allocated cell
 */
cell_t *LL_create_cell(monom_t *monom)
{
    cell_t *cell = NULL;

    if (monom)
    {
        cell = malloc(sizeof(cell_t));
        if (cell)
        {
            cell->val = *monom; // copy the monom
            cell->next = NULL;
        }
    }
    return cell;
}

/**
 * @brief Insert a cell into a linked list at the given position
 * @param [in, out] pprev address of previous pointer of the cell
 * @param [in] insert address of the cell to be added to the linked list
 * @details pprev and insert must not to be NULL
 * @note Lexique: None
 */
void LL_add_cell(cell_t **pprev, cell_t *insert)
{
    if (pprev && insert)
    {
        LL_free_list(&insert->next); // pprev -> A | insert -> NULL
        insert->next = *pprev;       // pprev -> A | insert -> A
        *pprev = insert;             // pprev -> insert -> A
    }
}

/**
 * @brief Create a linked list from a file (in which the data is sorted)
 * @param [in, out] adrhead address of head pointer of a linked list
 * @param [in] file file pointer of a file containing the data for a linked list (closed at end)
 * @return head pointer of the loaded linked list
 * @details if adrhead points to a list, it will be freed before being replaced by the loaded one (always)
 * @details if adrhead is NULL, the loaded list will only be returned
 * @details if file is NULL, always returns a void list
 * @note Lexique:
 *          - head: the head pointer of the newly loaded list
 *          - pprev: address of head pointer
 *          - monom: buffer used to load monom data
 *          - run: simulate break
 */
cell_t *LL_create_list_fromFile(cell_t **adrhead, FILE *file)
{
    LL_free_list(adrhead); // reset the list if any

    cell_t *head = NULL,
           **pprev = &head;

    if (file)
    {
        monom_t monom;
        char run = 1;

        while (run && fscanf(file, "%lf %u", &monom.coef, &monom.degree) == 2)
        {
            LL_add_cell(pprev, LL_create_cell(&monom)); // create and insert, if create fails with NULL, insert nothing and break the loop

            if (*pprev == NULL)
                run = 0; // Break
            else
                pprev = &(*pprev)->next; // Forward pprev
        }
        fclose(file); // Always close the file (helpful later)
    }

    if (adrhead)
        *adrhead = head; // Also return the loaded list by argument if any

    return head;
}

/**
 * @brief Create a linked list from a file (in which the data is sorted): see LL_create_list_fromFile
 * @param [in, out] adrhead address of head pointer of a linked list
 * @param [in] filename name of a file containing the data for a linked list
 * @return head pointer of the loaded linked list
 * @details if adrhead points to a list, it will be freed before being replaced by the loaded one (always)
 * @details if adrhead is NULL, the loaded list will only be returned
 * @details if filename is NULL or invalid, always returns a void list
 * @note Lexique: None
 */
cell_t *LL_create_list_fromFileName(cell_t **adrhead, const char *const filename)
{
    return LL_create_list_fromFile(adrhead, filename ? fopen(filename, "r") : NULL); // file closed by LL_create_list_fromFile -> OK
}

/**
 * @brief Write the linked list to an output stream
 * @param [in] file file pointer of an output stream (not closed at end)
 * @param [in] head head pointer of a linked list
 * @param [in] callback fonction pointer for printing the data of a cell on an output stream
 * @details if head or file are NULL, nothing is done
 * @details if callback is NULL, monom_save2file will be used
 * @note Lexique:
 *          - curr: used to iter over the list
 */
void LL_save_list_toFile(FILE *file, cell_t *head, void(*callback)(FILE *, monom_t *))
{
    if (head && file)
    {
        if (!callback) // Unspecified callback is changed to the default one
            callback = monom_save2file;

        for (cell_t *curr = head; curr; curr = curr->next) // this is a while in for-form (prettier \(^^)/)
        {
            callback(file, &curr->val);
        }
    }
}

/**
 * @brief Save a linked list into a file
 * @param [in, out] head head pointer of a linked list
 * @param [in] filename name of the backup file
 * @param [in] callback fonction pointer for writing the data of a cell to an output stream
 * @details if head or filename are NULL, nothing is done
 * @details if callback is NULL, monom_save2file will be used
 * @note Lexique:
 *          - file: file handler used to write data
 */
void LL_save_list_toFileName(const char *const filename, cell_t *head, void(*callback)(FILE *, monom_t *))
{
    if (head && filename)
    {
        FILE *file = fopen(filename, "w");

        if (file)
        {
            LL_save_list_toFile(file, head, callback);
            fclose(file);
        }
    }
}

/**
 * @brief Search a value in a linked list, and return the address of the previous pointer
 * @param [in] adrhead address of the head pointer
 * @param [in] mn address of the value to search
 * @param [in] monom_cmp fonction pointer for comparison of two values
 * @return the address of the previous pointer
 * @details if adrhead or mn is NULL, NULL is returned
 * @details if monom_cmp is NULL, monom_degree_cmp will be used
 * @note Lexique:
 *          - pprev: address of head pointer
 */
cell_t **LL_search_prev(cell_t **adrhead, monom_t *mn, int(*monom_cmp)(monom_t *, monom_t *))
{
    cell_t **pprev = adrhead;
    if (pprev && mn)
    {
        if (!monom_cmp) // Unspecified callback is changed to the default one
            monom_cmp = monom_degree_cmp;

        while (*pprev && monom_cmp(&(*pprev)->val, mn) < 0) // ~here: *pprev -> ... -> target
            pprev = &(*pprev)->next;
    }
    return pprev; // ~here: *pprev >= target OR *pprev = NULL (or pprev = NULL if adrhead NULL)
}

/**
 * @brief Delete a cell from a linked list
 * @param [in, out] pprev address of the previous pointer of the cell to delete
 * @details if pprev is NULL or points to nothing, nothing will be done
 * @note Lexique:
 *          - del: used to store the pointer of the cell to delete
 */
void LL_del_cell(cell_t **pprev)
{
    if (pprev && *pprev)
    {
        cell_t *del = *pprev;    // A -> *pprev -> B
        *pprev = (*pprev)->next; // A -> B
        free(del);
    }
}

/**
 * @brief Free the memory location occupied by a linked list
 * @param [in, out] adrhead address of head pointer of a linked list
 * @details if adrhead is NULL, nothing will be done
 * @note Lexique:
 *          - pprev: address of head pointer
 */
void LL_free_list(cell_t **adrhead)
{
    if (adrhead)
    {
        cell_t **pprev = adrhead; // pprev: H -> Q1 -> Q2 -> ... -> NULL
        while (*pprev)
            LL_del_cell(pprev); // pprev: Qi -> Qi+1 -> ... -> NULL
    }                           // pprev: NULL
}
