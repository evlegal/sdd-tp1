/**
 * @file polynomial.c
 * @brief Implementation file of polynomial module
 */

#include "linkedList.h"

/**
 * @brief compute 'in place' the derive of a polynomial
 * @param [in, out] adrhead address of a polynomial's adrhead pointer
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @details output only if there are not any error
 * @note Lexique:
 *          - cell: address of cell (used as iterator)
 */
void poly_derive(cell_t **adrhead, FILE *output)
{
    if (adrhead)
    {
        if (*adrhead && (*adrhead)->val.degree == 0) // delete monom of degree 0
        {
            LL_del_cell(adrhead);
        }

        for (cell_t *cell = *adrhead; cell; cell = cell->next)
        {
            cell->val.coef *= cell->val.degree; // derivation: (coef) * x ^ (n) -> (coef * n) * x ^ (n - 1)
            cell->val.degree--;
        }

        LL_save_list_toFile(output ? output : stdout, *adrhead, NULL); // Q5 Bonus
    }
}

/**
 * @brief compute P1 = P1 + P2, P2 become empty
 * @param [in, out] P1 address of the 1st polynomial's head pointer
 * @param [in, out] P2 address of the 2nd polynomial's head pointer
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @details if any P1 or P2 is NULL, nothing is done
 * @details output only if there are not any error
 * @note Lexique:
 *          - currt: address of cell (used as tmp)
 *          - pprev1: address of a P1's cell pointer, used to iter over P1
 *          - pprev2: address of a P2's cell pointer, used to iter over P2
 */
void poly_add(cell_t **P1, cell_t **P2, FILE *output)
{
    if (P1 && P2)
    {
        cell_t *currt,
            **pprev1 = P1,
            **pprev2 = P2;

        while (*pprev1 && *pprev2)
        {
            switch (monom_degree_cmp(&(*pprev1)->val, &(*pprev2)->val))
            {
            case -1:
                // Degree1 < Degree2

                pprev1 = &(*pprev1)->next; // Forward pprev1
                break;

            case 0:
                // Degree1 = Degree2

                (*pprev1)->val.coef += (*pprev2)->val.coef; // Add Coef: Coef1 = Coef1 + Coef2

                if ((*pprev1)->val.coef == 0) // Monom with coef = 0 must be deleted
                    LL_del_cell(pprev1);      // Delete cell & forward pprev1
                else
                    pprev1 = &(*pprev1)->next; // forward pprev1

                LL_del_cell(pprev2); // Delete & forward pprev2
                break;

            case 1:
                // Degree1 > Degree2

                currt = (*pprev2)->next;
                (*pprev2)->next = NULL; // avoid free in LL_add_cell

                LL_add_cell(pprev1, *pprev2); // Add the cell pointed by pprev2 to P1'

                pprev1 = &(*pprev1)->next; // Forward pprev1
                *pprev2 = currt;           // Forward pprev2
                break;
            }
        }
        if (!(*pprev1)) // If iteration aver P1 ends, move the rest of P2 at the ends of P1
            *pprev1 = *pprev2;

        *pprev2 = NULL;
        LL_save_list_toFile(output ? output : stdout, *P1, NULL); // Q5 Bonus
    }
}

/**
 * @brief compute P1 * P2
 * @param [in] P1 head pointer of the 1st polynomial
 * @param [in] P2 head pointer of the 2nd polynomial
 * @param [in, out] output optional file to output, stdout by default (if NULL)
 * @return P3 = P1 * P2
 * @details if any P1 or P2 is NULL, NULL is returned
 * @details if a memory alloc fails, NULL is returned
 * @details P1 and P2 are never altered
 * @details output only if there are not any error
 * @note Lexique:
 *          - P3: head pointer of the returned product
 *          - curr1: cell pointer, used as iterator over P1
 *          - curr2: cell pointer, used as iterator over P2
 *          - pprev3: address of a cell pointer, used as iterator over &P3
 *          - insert: used when inserting a new cell
 *          - pprev: used to keep order of monoms in P3
 *          - mn: monom used to compute monom of the product
 *          - alloc_error: used to interrupt the process at any alloc error
 */
cell_t *poly_prod(cell_t *P1, cell_t *P2, FILE *output)
{
    cell_t *P3 = NULL,
           *insert,
           **pprev;

    monom_t mn;

    char alloc_error = 0; // "breaks" loop if malloc error

    for (cell_t *curr1 = P1; curr1 && !alloc_error; curr1 = curr1->next)
    {

        for (cell_t *curr2 = P2; curr2 && !alloc_error; curr2 = curr2->next)
        {

            mn.coef = curr1->val.coef * curr2->val.coef,
            mn.degree = curr1->val.degree + curr2->val.degree; // For each monom from P1 & P2, make all monoms products

            pprev = LL_search_prev(&P3, &mn, NULL); // Search where to insert the product

            if (*pprev && (*pprev)->val.degree == mn.degree) // If a monom of same degree is found: merge them (better than calling monom_degree_cmp here)
            {
                (*pprev)->val.coef += mn.coef;
            }
            else // Else, insert it at place indicated by pprev
            {
                insert = LL_create_cell(&mn);
                LL_add_cell(pprev, insert); // safe if insert is NULL

                if (!insert) // Malloc error
                    alloc_error = 1;
            }
        }
    }

    if (alloc_error)
        LL_free_list(&P3); // Avoid returning a bad product
    else
    {
        for (cell_t **pprev3 = &P3; *pprev3;) // Removes monoms with coef = 0
        {
            if (!(*pprev3)->val.coef)
                LL_del_cell(pprev3);
            else
                pprev3 = &(*pprev3)->next;
        }

        LL_save_list_toFile(output ? output : stdout, P3, NULL); // Q5: Bonus
    }

    return P3;
}